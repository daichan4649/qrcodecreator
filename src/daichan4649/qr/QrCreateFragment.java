package daichan4649.qr;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;

import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;

public class QrCreateFragment extends SherlockFragment {

    public static QrCreateFragment newInstance() {
        return new QrCreateFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    private EditText mUrlView;
    private ImageView mQrCodeView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_qr_create, container, false);
        mUrlView = (EditText) view.findViewById(R.id.url);
        mQrCodeView = (ImageView) view.findViewById(R.id.qrcode);
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.fragment_qr_create, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case R.id.menu_qr:
            startQrCodeTask();
            return true;
        default:
            return super.onOptionsItemSelected(item);
        }
    }

    private QrCodeTask mTask;

    private void startQrCodeTask() {
        // IME閉じる
        final InputMethodManager imm = (InputMethodManager) getActivity()
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);

        // url(入力値)
        String url = mUrlView.getText().toString();
        if (TextUtils.isEmpty(url)) {
            return;
        }

        // タスク開始
        if (mTask != null && !mTask.isCancelled()) {
            mTask.cancel(true);
        }
        mTask = new QrCodeTask();
        mTask.execute(url);
    }

    private class QrCodeTask extends AsyncTask<String, Void, Bitmap> {

        @Override
        protected void onPreExecute() {
            mQrCodeView.setImageBitmap(null);
        }

        @Override
        protected Bitmap doInBackground(String... params) {
            return QrUtil.createQrCode(params[0]);
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            mQrCodeView.setImageBitmap(result);
        }
    }
}
