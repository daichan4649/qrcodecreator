package daichan4649.qr;

import android.graphics.Bitmap;
import android.graphics.Color;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

public class QrUtil {

    private static final int QR_WIDTH = 300;
    private static final int QR_HEIGHT = 300;

    public static Bitmap createQrCode(final String contents) {
        // 参考
        // http://dev.classmethod.jp/smartphone/android/android-tips-42-zxing-lib/
        try {
            // encode
            BitMatrix matrix = null;
            QRCodeWriter writer = new QRCodeWriter();
            matrix = writer.encode(contents, BarcodeFormat.QR_CODE, QR_WIDTH, QR_HEIGHT);

            // データがあるところだけ黒にする
            int width = matrix.getWidth();
            int height = matrix.getHeight();
            int[] pixels = new int[width * height];
            for (int y = 0; y < height; y++) {
                int offset = y * width;
                for (int x = 0; x < width; x++) {
                    pixels[offset + x] = matrix.get(x, y) ? Color.BLACK : Color.WHITE;
                }
            }
            Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            bitmap.setPixels(pixels, 0, width, 0, 0, width, height);
            return bitmap;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
